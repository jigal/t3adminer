.. include:: /Includes.rst.txt

.. _howtogethelp:

How to get help
===============

In most cases the extension will work automatically after installation (provided that your account is configured as
system maintainer).

If you encounter a bug you can report it at `the bug tracker`_.

For direct questions you can find me at the TYPO3 `slack`_ workspace.



.. _the bug tracker: https://gitlab.com/jigal/t3adminer/-/issues/
.. _slack: https://typo3.slack.com/team/U027KCK1E